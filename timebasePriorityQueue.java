package Snackdown_round1;

import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.* ; 
import com.opencsv.*; 
public class timebasePriorityQueue {

	 static class Node {  
	    String task;  
	    Date timestamp;
	    int priority;  
	    
	     Node next;  
	    
	} 
	  
	static Node node = new Node(); 
	    
	static Node newNode(String d, Date time, int p)  
	{  
	    Node temp = new Node();  
	    temp.task = d;
	    temp.timestamp=time;
	    temp.priority = p;  
	    temp.next = null;  
	    
	    return temp;  
	}  
	    
	// Return the value at head  
	static void peek(Node head)  
	{  
		System.out.println("taskname: "+head.task+"  timestamp: "+head.timestamp+"   priority: "+head.priority);

	}  
	    
	static Node pop(Node head)  
	{  
	    Node temp = head;  
	    (head)  = (head).next;  
	    return head; 
	}    
	    
	// Function to push according to priority  
	static Node push(Node head, String d, Date time,int p)  
	{  
	    Node start = (head);  
	    
	    Node temp = newNode(d, time, p); 

	    if ((head).priority >= p) { 
	    	if(start.timestamp.before(time)){
		        while (start.next != null && start.next.timestamp.after(time)) {  
		            start = start.next;  
		        }  
		        temp.next = start.next;  
		        start.next = temp;  
	    	}
	    	else{
		    	
		        temp.next = head;  
		        (head) = temp;
	    	}

	        
	    }  
	    else {  
	     
	        while (start.next != null &&  
	               start.next.priority < p && start.next.timestamp.after(time)) {  
	            start = start.next;  
	        }  
	        temp.next = start.next;  
	        start.next = temp;  

	    }  


	    return head; 
	}  
	    
	static int isEmpty(Node head)  
	{  
	    return ((head) == null)?1:0;  
	}  
	    
	// Driver code
	
	public static void main(String args[]) 
	{  
//uncomment if want to read from file
//		Scanner sc=new Scanner(System.in);
//		System.out.println("Please enter file path with file name");
//		String csvfile=sc.next();
        Node pq = null;
        try { 
//            FileReader filereader = new FileReader(csvfile); 
//  
//            CSVReader csvReader = new CSVReaderBuilder(filereader) 
//                                      .withSkipLines(1) 
//                                      .build(); 
//            List<String[]> allData = csvReader.readAll(); 

            int i=0;
            int data,priority;
            Date time;
            
//            for (String[] row : allData) {
//            	if(i==0){
//            		data=Integer.parseInt(row[0]);
//            		time=new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(row[1]);
//            		
//            		if(row[2]==null){
//            			priority=Integer.MAX_VALUE;
//            		}
//            		else{
//            			priority=Integer.parseInt(row[2]);
//            		}
//                    pq = newNode(data, time,priority);
//            	}
//            	else{
//            		data=Integer.parseInt(row[0]);
//            		time=new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(row[1]);
//            		if(row[2]==null){
//            			priority=Integer.MAX_VALUE;
//            		}
//            		else{
//            			priority=Integer.parseInt(row[2]);
//            		}
//                    pq = push(pq,data, time,priority);
//            	}
//
//            } 
            pq=newNode("#task501", new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2017/02/10 5:01"), 2);
            pq=push(pq,"#task502", new SimpleDateFormat("yyyy/MM/dd hh:mm").parse("2017/02/10 5:01"), 1);
            pq=push(pq, "#task503",new SimpleDateFormat("yyyy/MM/dd hh:mm").parse("2017/02/10 5:00"), 1);
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        } 
        
	    
	    while (isEmpty(pq)==0) {  
	        peek(pq);  
	        pq=pop(pq);  
	    }  
	    
	}  
	} 
